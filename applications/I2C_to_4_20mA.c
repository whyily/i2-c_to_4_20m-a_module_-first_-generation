/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-10-11     lzy       the first version
 */
#define LOG_TAG     "I2C_to_4_20mA"       // 该模块对应的标签。不定义时，默认：NO_TAG
#define LOG_LVL     LOG_LVL_DBG         // 该模块对应的日志输出级别。不定义时，默认：调试级别
#include <ulog.h>                       // 必须在 LOG_TAG 与 LOG_LVL 下面


#include "I2C_to_4_20mA.h"

#include <rtthread.h>
#include <rtdevice.h>
#include <math.h>

#define I2C_BUS ("i2c2")
#define GP8212S_ADDR (0x58)     //i2c 地址

#define MAX_K  (1.5f)
#define MIN_K  (0.5f)

#define MAX_B   (6.0f)
#define MIN_B   (2.0f)

struct rt_i2c_bus_device *i2c_bus;      /* I2C总线设备句柄 */
static double k = 1.0f;    //斜率
static double b = 4.0f;    //偏置

/**
  * @brief  0~25mA init
  * @param  none
  * @return none
  */
static void the_0to25mA_init(void){

    rt_thread_mdelay(100);

    /* 查找I2C总线设备，获取I2C总线设备句柄 */
    i2c_bus = (struct rt_i2c_bus_device *)rt_device_find(I2C_BUS);
}


/**
  * @brief  set 0~25mA output
  * @param  current 0~25mA
  * @return none
  */
static int set_0to25mA_output(double current)
{
    struct rt_i2c_msg msgs;

    unsigned char buf[3];
    unsigned short data;

    //计算输出电流
    data = (unsigned short)round((current /(2.5/0.1)) * 32768.0f);

    //输出范围限制
    if(data > 0x7FFF){
        data = 0x7FFF;
    }


    buf[0] = 0x02;
    buf[1] = data & 0x00ff;  //低位
    buf[2] = data >> 8;      //高位

    msgs.addr = GP8212S_ADDR;         /* 从机地址 */
    msgs.flags = RT_I2C_WR;   /* 读标志 */
    msgs.buf =  buf;           /* 读写数据缓冲区指针　*/
    msgs.len = 3;             /* 读写数据字节数 */

    /* 调用I2C设备接口传输数据 */
    if (rt_i2c_transfer(i2c_bus, &msgs, 1) == 1)
    {
        //LOG_D("wrtie 0~25mA ok!\n");
        return RT_EOK;
    }
    else
    {
        //LOG_D("wrtie 0~25mA fail!\n");
        return -RT_ERROR;
    }
}



/**
  * @brief  set 4~20mA output
  * @param  x 0~16  0- 4mA  16- 20mA
  * @return none
  */
static void set_4to20mA_output(double x)
{
    double y;

    //参数范围限制
    if(x < 0.0f){
        x = 0.0f;
    }

    if(x > 16.0f){
        x = 16.0f;
    }

    //计算实际输出
    y = k*x + b;

    //设置输出
    set_0to25mA_output(y);
}


/**
  * @brief  4~20mA moudle init
  * @param  none
  * @return none
  */
void the_4to20mA_moudle_init(void){
    the_0to25mA_init();
}


/**
  * @brief  the_4to20mA_moudle_run
  * @param  x 0~16  0- 4mA  16- 20mA
  * @param  mode 0.normal  1.zero  2.full
  * @return none
  */
void the_4to20mA_moudle_run(double x,unsigned char mode){

    switch(mode){
    case 0: //normal
        set_4to20mA_output(x);

        //LOG_D("set_4to20mA_output %fmA\n",x + 4.0);
        break;

    case 1: //4mA
        set_4to20mA_output(0.0f);

        //LOG_D("set_4to20mA_output 4mA\n");
        break;

    case 2: //20mA
        set_4to20mA_output(16.0f);

        //LOG_D("set_4to20mA_output 20mA\n");
        break;

    default:
        break;
    }
}


/**
  * @brief  Zero point parameter adjustment up
  * @param  none
  * @return none
  */
static void the_4to20mA_zero_point_paramter_adjustment_up(void){

    b = b + 0.0001;

    if(b > MAX_B){
        b = MAX_B;
    }

    LOG_D("the 4to20mA  b = %f\n",b);
}
MSH_CMD_EXPORT(the_4to20mA_zero_point_paramter_adjustment_up , the_4to20mA_zero_pint_paramter_adjustment_up);


/**
  * @brief  Zero point parameter adjustment down
  * @param  none
  * @return none
  */
void the_4to20mA_zero_point_paramter_adjustment_down(void){

    b = b - 0.0001;

    if(b < MIN_B){
        b = MIN_B;
    }

    LOG_D("the 4to20mA  b = %f\n",b);
}
MSH_CMD_EXPORT(the_4to20mA_zero_point_paramter_adjustment_down , the_4to20mA_zero_pint_paramter_adjustment_down);


/**
  * @brief  Full scale parameter adjustment up
  * @param  none
  * @return none
  */
void the_4to20mA_full_scale_parameter_adjustment_up(void){

    k = k + 0.00001;

    if(k > MAX_K){
        k = MAX_K;
    }

    LOG_D("the 4to20mA  k = %f\n",k);
}
MSH_CMD_EXPORT(the_4to20mA_full_scale_parameter_adjustment_up , the_full_scale_parameter_adjustment_up);


/**
  * @brief  Full scale parameter adjustment down
  * @param  none
  * @return none
  */
void the_4to20mA_full_scale_parameter_adjustment_down(void){

    k = k - 0.00001;

    if(k < MIN_K){
        k = MIN_K;
    }

    LOG_D("the 4to20mA  k = %f\n",k);
}
MSH_CMD_EXPORT(the_4to20mA_full_scale_parameter_adjustment_down , the_full_scale_parameter_adjustment_down);

