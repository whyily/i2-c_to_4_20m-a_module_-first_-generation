/*
 * Copyright (c) 2006-2023, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-10-10     RT-Thread    first version
 */

#define LOG_TAG     "main"       // 该模块对应的标签。不定义时，默认：NO_TAG
#define LOG_LVL     LOG_LVL_DBG         // 该模块对应的日志输出级别。不定义时，默认：调试级别
#include <ulog.h>                       // 必须在 LOG_TAG 与 LOG_LVL 下面



#include <rtthread.h>
#include <rtdevice.h>
#include <stdlib.h>

#include "I2C_to_4_20mA.h"


double x = 0.0f;
unsigned char mode = 0;

/**
  * @brief  set_4to20mA_moudle_to_normal_mode
  * @param  none
  * @return none
  */
static void set_4to20mA_moudle_to_normal_mode(void){
    mode = 0;

    LOG_D("set_4to20mA_output normal mode\n");
}
MSH_CMD_EXPORT(set_4to20mA_moudle_to_normal_mode , set_4to20mA_moudle_to_normal_mode);


/**
  * @brief  set_4to20mA_moudle_to_zero_mode
  * @param  none
  * @return none
  */
static void set_4to20mA_moudle_to_zero_mode(void){
    mode = 1;

    LOG_D("set_4to20mA_output 4mA mode\n");
}
MSH_CMD_EXPORT(set_4to20mA_moudle_to_zero_mode , set_4to20mA_moudle_to_zero_mode);


/**
  * @brief  set_4to20mA_moudle_to_full_mode
  * @param  none
  * @return none
  */
static void set_4to20mA_moudle_to_full_mode(void){
    mode = 2;

    LOG_D("set_4to20mA_output 20mA mode\n");
}
MSH_CMD_EXPORT(set_4to20mA_moudle_to_full_mode , set_4to20mA_moudle_to_full_mode);


/**
  * @brief  set_4to20mA_output_current
  * @param  none
  * @return none
  */
static void set_4to20mA_output_current(int argc, char**argv)
{
    if (argc < 2)
    {
        rt_kprintf("Please input'set_4to20mA_current <0.0~16.0>'\n");
        return;
    }

    x = atof(argv[1]);

    LOG_D("set_4to20mA_current x = %f\n",x);

}
MSH_CMD_EXPORT(set_4to20mA_output_current, set_4to20mA_output_current sample: set_4to20mA_output_current <0.0~16.0>);


int main(void)
{

    the_4to20mA_moudle_init();

    while (1)
    {

        the_4to20mA_moudle_run(x,mode);

        rt_thread_mdelay(10);
    }

    return RT_EOK;
}
