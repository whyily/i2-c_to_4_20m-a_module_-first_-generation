/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-10-11     lzy       the first version
 */
#ifndef I2C_TO_4_20MA_H_
#define I2C_TO_4_20MA_H_

void the_4to20mA_moudle_init(void);
void the_4to20mA_moudle_run(double x,unsigned char mode);


#endif /* I2C_TO_4_20MA_H_ */
